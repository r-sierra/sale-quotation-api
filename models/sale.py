# -*- coding: utf-8 -*-

from odoo import fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    payment_transaction = fields.Char(
        string='Payment Transaction',
        copy=False,
        readonly=True
    )
