# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* sale_quotation_api
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 13.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-05 03:59+0000\n"
"PO-Revision-Date: 2021-11-05 03:59+0000\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager_sale_order__create_uid
msgid "Created by"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager_sale_order__create_date
msgid "Created on"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager__display_name
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager_sale_order__display_name
msgid "Display Name"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager__id
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager_sale_order__id
msgid "ID"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager____last_update
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager_sale_order____last_update
msgid "Last Modified on"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager_sale_order__write_uid
msgid "Last Updated by"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_onchange_manager_sale_order__write_date
msgid "Last Updated on"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model.fields,field_description:sale_quotation_api.field_sale_order__payment_transaction
msgid "Payment Transaction"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model,name:sale_quotation_api.model_sale_order
msgid "Sales Order"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model,name:sale_quotation_api.model_onchange_manager
msgid "onchange.manager"
msgstr ""

#. module: sale_quotation_api
#: model:ir.model,name:sale_quotation_api.model_onchange_manager_sale_order
msgid "onchange.manager.sale.order"
msgstr ""
