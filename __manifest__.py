# -*- coding: utf-8 -*-
{
    'name': "Cotizaciones de venta",
    'summary': "Crear cotizaciones de venta mediante API",
    'author': "Roberto Sierra <roberto@ideadigital.com.ar>",
    'website': "https://gitlab.com/r-sierra",
    'category': 'Sales/Sales',
    'version': '0.1',
    'depends': [
        'sale',
    ],
    'data': [
        'views/sale_order_views.xml',
    ],
    'license': 'LGPL-3',
}
