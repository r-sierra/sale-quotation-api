# -*- coding: utf-8 -*-
import logging
from werkzeug.exceptions import UnprocessableEntity

import odoo.exceptions
from odoo import http
from odoo.http import request
from odoo.tools.float_utils import float_repr, float_round

_logger = logging.getLogger(__name__)

REQUIRED_ORDER_FIELDS = [
    'ref',
    'date',
    'lines',
    'customerId',
]
REQUIRED_ORDER_LINE_FIELDS = [
    'sku',
    'qty'
]
REQUIRED_CONFIRM_FIELDS = [
    'transactionId'
]

def float_round_repr(value, decimals=2):
    return float_repr(float_round(value, decimals), decimals)


class Main(http.Controller):
    @http.route('/sale_quotation_api/sale',
                type='json',
                auth='user',
                methods=['POST'],
                cors='*',
                csrf=False
                )
    def index(self, **kwargs):
        data = request.jsonrequest
        _logger.info('POST /sale_quotation_api/sale params: %s' % data)

        params = self._require_fields(data, REQUIRED_ORDER_FIELDS)

        params['lines'] = []  # Sobreescribir el valor original
        for line in data['lines']:
            r = self._require_fields(line, REQUIRED_ORDER_LINE_FIELDS)
            params['lines'].append(r)

        vals = self._prepare_order_vals(**params)
        onchange_model = request.env['onchange.manager.sale.order']
        values = onchange_model.play(vals, vals['order_line'])

        context = {'force_company': request.env.company.id}
        model_ctx = request.env['sale.order'].sudo().with_context(context)
        try:
            record = model_ctx.create(values)
        except odoo.exceptions.ValidationError as e:
            raise UnprocessableEntity('Unable to create Order. Error: %s' % e)

        record.ensure_one()

        return {
            'id': record.id,
            'ref': record.client_order_ref,
            'date': record.date_order,
            'customerId': record.partner_id.id,
            'lines': self._get_order_line_vals(record),
            'amount_untaxed': float_round_repr(record.amount_untaxed),  # Total without taxes
            'amount_tax': float_round_repr(record.amount_tax),  # Total taxes
            'amount_total': float_round_repr(record.amount_total),  # Total with taxes
            'amount_undiscounted': float_round_repr(record.amount_undiscounted),  # Total without taxes before discount
        }

    @http.route("/sale_quotation_api/sale/<model('sale.order'):sale>/confirm",
                type='json',
                auth='user',
                methods=['POST'],
                cors='*',
                csrf=False
                )
    def confirm(self, sale, **kwargs):
        data = request.jsonrequest
        params = self._require_fields(data, REQUIRED_CONFIRM_FIELDS)
        sale.write({'payment_transaction': params['transactionId']})

        sale.action_confirm()
        return sale.read(['name', 'state'])

    def _get_order_line_vals(self, sale):
        vals = []
        for line in sale.order_line:
            vals.append({
                'code': line.product_id.default_code,
                'sku': line.product_id.barcode,
                'price': float_round_repr(line.price_unit),
                'discount': float_round_repr(line.discount),
                'qty': line.product_uom_qty,
                'price_tax': float_round_repr(line.price_tax),  # Total taxes
                'price_subtotal': float_round_repr(line.price_subtotal),  # Total without taxes
                'price_total': float_round_repr(line.price_total),  # Total with taxes
            })
        return vals

    def _require_fields(self, data, fields):
        params = {}
        for field in fields:
            if isinstance(field, dict):
                for k, v in field.items():
                    params[k] = self._require_fields(data.get(k, {}), v)
            elif isinstance(field, str):
                try:
                    params[field] = data[field]
                except KeyError:
                    raise UnprocessableEntity('Missing param \'%s\'' % field)
                if isinstance(params[field], float) or isinstance(params[field], int):
                    pass  # floats and int can be zero!
                elif not params[field]:
                    raise UnprocessableEntity('Empty param \'%s\'' % field)
        return params

    def _get_partner(self, partner_id):
        partner_id = request.env['res.partner'].browse(partner_id)
        if not partner_id.id:
            raise UnprocessableEntity('Partner not found \'%s\'' % partner_id)
        return partner_id

    def _get_product(self, sku):
        domain = [
            '|',
            ('default_code', '=', sku),
            ('barcode', '=', sku),
            ('sale_ok', '=', True),
            '|',
            ('company_id', '=', False),
            ('company_id', '=', request.env.company.id)
        ]
        product_id = request.env['product.product'].search(domain, limit=1)
        if not product_id.id:
            raise UnprocessableEntity('Product not found \'%s\'' % sku)
        return product_id

    def _prepare_order_line(self, line):
        return {
            'product_id': self._get_product(line['sku']).id,
            'product_uom_qty': line['qty'],
        }

    def _prepare_order_vals(self, ref, date, customerId, lines):
        vals = {
            'client_order_ref': ref,
            'date_order': date,
            'partner_id': self._get_partner(customerId).id,
            'order_line': [(0, 0, self._prepare_order_line(line)) for line in lines],
        }
        return vals
